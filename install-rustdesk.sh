#!/bin/bash

echo "Iniciando instalação RustDesk (AppImage)"
# Baixando o arquivo AppImage
wget https://github.com/rustdesk/rustdesk/releases/download/1.2.3-1/rustdesk-1.2.3-1-x86_64.AppImage -P /home/$USER/Downloads/

# Renomeando o arquivo baixado para Rustdesk
mv /home/$USER/Downloads/rustdesk-1.2.3-1-x86_64.AppImage /home/$USER/Downloads/Rustdesk

# Dando permissão de execução ao arquivo
chmod +x /home/$USER/Downloads/Rustdesk

# Movendo o arquivo para a Área de Trabalho
mv /home/$USER/Downloads/Rustdesk /home/$USER/Área\ de\ Trabalho/

echo "Executado!"
